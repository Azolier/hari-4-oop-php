<?php
    // require('animal.php');
    // require('frog.php');
    require('ape.php');

    
    $sheep = new Animal("Shaun");

    echo "Nama Hewan : " . $sheep->name . "<br>"; // "shaun"
    echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
    echo "Berdarah Dingin : " . $sheep->cold_blooded. "<br>"; // "no"
    echo "<br>";

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded()).

    $kodok = new Kodok("buduk");
    echo "Nama Hewan : " . $kodok->name . "<br>"; // "shaun"
    echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 4
    echo "Berdarah Dingin : " . $kodok->cold_blooded. "<br>"; // "no"
    $kodok->jump();
    echo "<br>";

    $kong = new Kong("Kera Sakti");
    echo "Nama Hewan : " . $kong->name . "<br>"; // "shaun"
    echo "Jumlah Kaki : " . $kong->legs . "<br>"; // 4
    echo "Berdarah Dingin : " . $kong->cold_blooded. "<br>"; // "no"
    $kong->yell();
    echo "<br>";

?>